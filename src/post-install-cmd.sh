#!/usr/bin/env bash

echo "post-install-cmd {projeto}"
echo "Verificando se existe a pasta mainapp instalada"

if [ -e /var/www/html/mainapp/br/gov/mainapp/application/{projeto} ]; then
    echo "Link /var/www/html/mainapp/br/gov/mainapp/application/{projeto} já existe .... [ OK ]"
else
    echo "Criando link /var/www/html/mainapp/br/gov/mainapp/application/{projeto} ..."
    ln -s /var/www/html/{projeto}/br/gov/mainapp/application/{projeto}/ /var/www/html/mainapp/br/gov/mainapp/application/{projeto}
    ls -la /var/www/html/mainapp/br/gov/mainapp/application/{projeto}
fi

if [ -e /var/www/html/mainapp/br/gov/mainapp/public/media/sisicmbio/{projeto} ]; then
    echo "Link /var/www/html/mainapp/br/gov/mainapp/public/media/sisicmbio/{projeto} já existe .... [ OK ]"
else
    echo "Criando link /var/www/html/mainapp/br/gov/mainapp/public/media/sisicmbio/{projeto} ..."
    ln -s /var/www/html/{projeto}/br/gov/mainapp/public/media/sisicmbio/{projeto}/ /var/www/html/mainapp/br/gov/mainapp/public/media/sisicmbio/{projeto}
    ls -la /var/www/html/mainapp/br/gov/mainapp/public/media/sisicmbio/{projeto}
fi

if [ -e /var/www/html/mainapp/br/gov/mainapp/data/{projeto} ]; then
    echo "Pasta data /var/www/html/mainapp/br/gov/mainapp/data/{projeto} já existe .... [ OK ]"
else
    echo "Criando pasta data da aplicacao /var/www/html/mainapp/br/gov/mainapp/data/{projeto} ..."
    mkdir -p /var/www/html/mainapp/br/gov/mainapp/data/{projeto}/
fi

chown apache:apache /var/www/html -R