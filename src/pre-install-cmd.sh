#!/usr/bin/env bash
echo "infoconv :: pre-install-cmd.sh"
echo "Verificando se existe a pasta mainapp instalada!"

if [ -e /var/www/html/mainapp/ ]; then
    echo "Existe pasta mainapp [ OK ]"
else
    echo "Pasta mainapp não existe."
    cd /var/www/html/
    git clone https://gitlab+deploy-token-49731:Rww6qPNp4fLTs9X27AKf@gitlab.com/sisicmbio/app_mainapp.git
    mv app_mainapp/src/ /var/www/html/mainapp/
    mv app_mainapp/conf/etc/httpd/conf.d/app_mainapp.conf  /etc/httpd/conf.d/app_mainapp.conf
    rm -rf app_mainapp
    cd /var/www/html/mainapp/
    pwd
    composer install --no-dev --optimize-autoloader
fi