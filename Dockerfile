FROM registry.gitlab.com/pdci/base_co7_httpd_php-5.6:apc_production  AS production
ARG PDCI_COMMIT_REF_NAME
ARG PDCI_COMMIT_SHA
ARG PDCI_COMMIT_SHORT_SHA
#ARG PDCI_COMMIT_MESSAGE
ARG PDCI_COMMIT_TAG
#ARG PDCI_COMMIT_TITLE
#ARG PDCI_COMMIT_DESCRIPTION

#LABEL Vendor="COTEC-LAF-SISBio"
#LABEL Description="Centos 7 , PHP 7.2, composer  "
LABEL PDCI_COMMIT_REF_NAME="${PDCI_COMMIT_REF_NAME}"
LABEL PDCI_COMMIT_SHA="${PDCI_COMMIT_SHA}"
LABEL PDCI_COMMIT_SHORT_SHA="${PDCI_COMMIT_SHORT_SHA}"
#LABEL PDCI_COMMIT_MESSAGE="${PDCI_COMMIT_MESSAGE}"
LABEL PDCI_COMMIT_TAG="${PDCI_COMMIT_TAG}"
#LABEL PDCI_COMMIT_TITLE="${PDCI_COMMIT_TITLE}"
#LABEL PDCI_COMMIT_DESCRIPTION="${PDCI_COMMIT_DESCRIPTION}"
#LABEL License=GPLv2
#LABEL Version=0.0.1

ENV APP_HOME /var/www/html/{projeto}
ENV APP_PROJETO "{projeto}"

RUN rm -rf /var/www/html/*
RUN mkdir -p $APP_HOME/
RUN rm -rf $APP_HOME/*

COPY conf/etc/httpd/conf.d/$APP_PROJETO.conf /etc/httpd/conf.d/$APP_PROJETO.conf
ADD run-httpd.sh /run-httpd.sh
RUN chmod -v +x /run-httpd.sh
COPY src/  $APP_HOME

RUN sed -i "s|;include_path = \".:/php/includes\"|include_path = \".:/usr/share/pear:/usr/share/php:$APP_HOME/vendor/zendframework/zendframework1/library\"|" /etc/php.ini
RUN sed -i "s|;date.timezone =|date.timezone=\"America/Sao_Paulo\"|" /etc/php.ini

WORKDIR $APP_HOME

RUN composer clearcache && \
    composer install --no-dev --optimize-autoloader

EXPOSE 80
VOLUME ["/var/lib/php/session","/var/www/html/mainapp/br/gov/mainapp/data/{projeto}/"]

CMD ["/run-httpd.sh"]

#FROM registry.gitlab.com/pdci/base_co7_httpd_php-5.6:testing AS testing
#
##COPY conf/etc/httpd/conf/httpd.conf  /etc/httpd/conf/httpd.conf
##COPY conf/laravel.conf /etc/httpd/conf.d/laravel.conf
#
#ENV APP_HOME /var/www/html
#
#ENV PDCI_SONARQUBE_URL_PORT=${PDCI_SONARQUBE_URL_PORT:-http://localhost:9000}
## Instalacao do node para os testes
#
#WORKDIR $APP_HOME
#
#RUN rm -rf $APP_HOME/*
#
#COPY src/  $APP_HOME
#
#COPY src/.env.app.dev $APP_HOME/.env
#
#WORKDIR $APP_HOME
#
##VOLUME $APP_HOME
#
#RUN composer clearcache && \
#  composer install && \
#  yarn install
#
#RUN chown apache:apache $APP_HOME -R
#RUN chmod 760 $APP_HOME -R
#
#CMD ["/run-httpd.sh"]
